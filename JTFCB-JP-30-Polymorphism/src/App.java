
public class App {

	public static void main(String[] args) {
		// polimorfizm oznacza ze jezeli masz klase dziecko jakiejs klasy matki
		// to mozesz uzyc tej klasy dziecko wszedzie gdzie chcialbys
		// uzyc tej klasy matki
		Plant plant1 = new Plant();
		Tree tree = new Tree();

		// dwie referencje na ten sam obiekt
		// Plant plant2 = plant1;
		// wiec moge zrobic zamiast tego tree
		// plant2 references a Tree, so the Tree grow() method is called.
		Plant plant2 = tree;

		plant2.grow();

		
		// The type of the reference decided what methods you can actually call;
        // we need a Tree-type reference to call tree-specific methods.
		tree.shedLeaves();

		// to juz nie zadziala
		// plant nie ma shedLeaves
		// plant2.shedLeaves();

		
		// to mozna spoko uzyc i bedzie tree growing
		// Another example of polymorphism.
		doGrow(tree);
	}

	public static void doGrow(Plant plant) {
		plant.grow();
	}
}
